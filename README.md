# Commbank Technical Frontend Test
Welcome to commonwealthbank technical ui test. The purpose of this assignment is to test your familiarity with JavaScript, HTML, DOM manipulation, browser performance and layout.

## Brief
In this repo there is a desktop and mobile image of a "card" we use on the commbank site.

* Develop the card as per the images supplied, in a responsive format.
* Lay 8 of this card out on the page in a grid. The grid should be responsive based on page width down to one column.
* On load the cards will need to animate in sequentially. There should be a 200ms gap between each cards animation. The type of animation is up to you.
